<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel Admin Panel

You can check from this link --> https://github.com/appzcoder/laravel-admin

## Screen shot

Dashboard Page

![Dashboard Page](img/dashboard.png "Dashboard Page")

Users Page

![Users Page](img/user1.png "Users Page")

![Users Page](img/user2.png "Users Page")

![Users Page](img/user3.png "Users Page")

Roles Page

![Roles Page](img/roles.png "Roles Page")

![Roles Page](img/roles1.png "Roles Page")

Permission Page

![Permission Page](img/permission.png "Permission Page")

Pages Page

![Pages Page](img/page.png "Pages Page")

![Pages Page](img/pages.png "Pages Page")

Logs Page

![Logs Page](img/logs.png "Logs Page")

Settings Page

![Settings Page](img/settings1.png "Settings Page")

![Settings Page](img/settings2.png "Settings Page")

Generator Page

![Generator Page](img/generator.png "Generator Page")
